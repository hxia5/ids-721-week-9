# IDS 721 Week 9
[![pipeline status](https://gitlab.com/hxia5/ids-721-week-9/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids-721-week-9/-/commits/main)

## Overview
* This is my repository ofIDS 721 Mini-Project 9 - Option 2: Streamlit App with a Hugging Face Model. I also deployed the model on AWS EC2 for extra credits.

## Purpose
- Create a website using Streamlit
- Connect to an open source LLM (Hugging Face)
- Deploy model via Streamlit or other service (accessible via browser, use EC2 for extra credits)

## EC2 Hosted Web App Link 
http://3.88.65.247:8501

## What The Model Does and How to Use It
- The model can identify whether there is a car in the image or not. For user, they can upload an image and the model will return the result of whether there is a car in the image or not, or use the default image provided by the app to test the model.

## SreenShot of Using My Streamlit App on Browser

![alt text](car.png)

![alt text](<no car.png>)


## Key Steps
1. Install streamlit
2. Choose your Hugging Face model, and get your api token and the api interface
3. Create an `.env` file and out your api token here
4. Edit `app.py` file to include your model and api token and create interface and logic for the streamlit app
5. Once you make the app do `streamlit run app.py` to test it locally on `http://localhost:8501`
6. Build `test_app.py` to test your code, and `.yml`file to test the pipeline after you push your code.
7. In AWS, search `EC2`
8. Click Launch Instance, choose Ubuntu, keep free one and t2.micro and also allow HTTP and allow HTTPS, then click edit to add security group rule, choose "CUSTOM TCP", port 8501, and choose "ANYWHERE" as source type, use the default setting for key pair, which is without key pair.
9.  Click Launch Instance
10. Click connect to instance 
11. In the popup cloud shell, set up the environment 
12. First git clone your repo
13. Run sudo apt update
14. Run sudo apt-get install -y python3-pip
15. ls to make sure your repo is here and cd to it
16. pip3 install -r requirements.txt
17. export PATH=~/.local/bin/:$PATH
18. pip install --upgrade jinja2 
19. run touch .env and then vi .env, paste your local .env here and then type ":wq" to save and exit back to the terminal
20. test your app by doing `streamlit run app.py`
21. Copy the `External URL` and paste it in your browser to see your app
22. To make sure your app is running after you close the cloud shell, run `screen streamlit run app.py` to run the app in the background
23. To exit the screen, type `ctrl+a` then `d`

## References
1. https://docs.streamlit.io/
2. https://discuss.streamlit.io/t/hosting-streamlit-with-aws-ec2/234


