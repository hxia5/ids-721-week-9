import os
from dotenv import load_dotenv
import requests
import base64


load_dotenv()

def test_api_key_loaded():
    """Check if the OpenAI API key is loaded from .env"""
    api = os.getenv(
        "API_TOKEN"
    )  # Ensure this matches the key name in your .env
    assert api is not None


API_TOKEN = os.getenv("API_TOKEN")

HEADERS = {"Authorization": f"Bearer {API_TOKEN}"}
API_URL = "https://api-inference.huggingface.co/models/google/vit-base-patch16-224"


def test_static_files(directory="static/"):
    """checks static files exists"""
    s_files = [f for f in os.listdir(directory) if f.endswith(".jpg")]

    for s_file in s_files:
        file_path = os.path.join(directory, s_file)
        assert os.path.exists(file_path) and os.path.isfile(file_path)


def test_api():
    """checks hugging api token works"""
    # Read an image file and encode it as base64
    with open("static/yangwang-u9.jpg", "rb") as file:
        image_data = file.read()
        encoded_image = base64.b64encode(image_data).decode("utf-8")

    # Construct the payload with the encoded image
    test_payload = {"inputs": {"image": encoded_image}}

    # Send the request to the API
    response = requests.post(API_URL, headers=HEADERS, json=test_payload)

    # Print response details for debugging
    print("Response status code:", response.status_code)
    print("Response content:", response.content)

    # Assert the status code is 200 (OK)
    assert response.status_code == 200



if __name__ == "__main__":
    test_api_key_loaded()
    test_static_files()
    test_api()
