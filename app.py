import streamlit as st
from dotenv import load_dotenv
import os
import requests
import base64
import json

API_TOKEN = os.getenv("API_TOKEN")

HEADERS = {"Authorization": f"Bearer {API_TOKEN}"}
API_URL = "https://api-inference.huggingface.co/models/google/vit-base-patch16-224"


def query(filename, uploaded=True):
    """Query Hugging Face Inference API"""
    data = None
    if uploaded is False:
        with open(filename, "rb") as f:
            data = f.read()
    else:
        data = filename

    encoded_data = base64.b64encode(data).decode("utf-8")

    payload = {
        "inputs": {
            "question": "is there a car in the picture?",  # Update the question
            "image": encoded_data,
        }
    }

    response = requests.post(API_URL, headers=HEADERS, json=payload)
    return json.loads(response.content.decode("utf-8"))


def main():
    """Main entry point"""
    st.title("Car Detection")
    image = st.empty()
    placeholder = st.empty()
    uploaded_file = st.file_uploader("Choose a file")

    if st.button("Use Default Image"):
        image.image("static/yangwang-u9.jpg", caption="Sample Image")
        result = query("static/yangwang-u9.jpg", uploaded=False)
        if isinstance(result, list):
            car_presence_score = result[0]["score"] if result else 0
        elif isinstance(result, dict):
            car_presence_score = result.get("score", 0)
        else:
            car_presence_score = 0

        if car_presence_score > 0.5:  # Adjust threshold as needed
            placeholder.text("Car is present in the picture!")
        else:
            placeholder.text("No car detected in the picture.")

    if uploaded_file is not None:
        if uploaded_file.type.startswith("image"):
            image.image(uploaded_file, caption="User Uploaded Image")
            result = query(uploaded_file.getvalue())
            if isinstance(result, list):
                car_presence_score = result[0]["score"] if result else 0
            elif isinstance(result, dict):
                car_presence_score = result.get("score", 0)
            else:
                car_presence_score = 0

            if car_presence_score > 0.5:  # Adjust threshold as needed
                placeholder.text("Car is present in the picture!")
            else:
                placeholder.text("No car detected in the picture.")
        else:
            placeholder.text("The uploaded file is not an image.")



if __name__ == "__main__":
    load_dotenv()
    main()
